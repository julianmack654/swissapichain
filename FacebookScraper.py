import sys
from social_parsing.social_parsing import FacebookScraper, FacebookException
from Company import CompanyData
from fuzzywuzzy import fuzz
CUT_OFF1 = 80
CUT_OFF2 = 45

class FacebookScraperWrapper():
    def __init__(self, url, full_address=None, street_address=None):
        self.url = url
        self.original_loc = street_address #for validation
        self.google_loc = full_address #for validation
        self.data = {}

    def scrape(self):
        try:
            scraped = FacebookScraper(self.url)
            address = scraped.schema_fb_street_address
            #print(scraped.likes_fb)
            if validate(address, self.google_loc, self.original_loc, CUT_OFF1, CUT_OFF2):
                data = {}
                data["url"] = self.url
                data["reviews"] = scraped.schema_fb_reviews
                data["post_code"] = scraped.schema_fb_postcode
                data["rating"] = scraped.schema_fb_rating_agg
                data["fb_likes"] = scraped.likes_fb
                data["fb_follows"] = scraped.follows_fb
                data["tel_num"] = scraped.phone_from_fb_about
                data["email"] = scraped.email_from_fb_about
                data["website"] = scraped.website_from_fb_about
                self.data = data
        except FacebookException:
            print("FACEBOOK EXCEPTION!")
        return self.data
def validate(address, location1, location2, cut_off1, cut_off2, return_scores=False):
    result = False
    scores = {"first": fuzz.token_set_ratio(address, location1),
            "second": fuzz.token_set_ratio(address, location2)}
    #print("first:", address, "AND",location1, "gives: ", scores["first"])
    #print("second:", address, "AND",location2, "gives: ", scores["second"])
    if scores["first"] > cut_off1:
        result = True
    elif scores["second"] > cut_off2:
        result = True

    if return_scores:
        return result, scores
    else:
        return result


if __name__ == "__main__":
    fb_company = FacebookScraperWrapper("https://www.facebook.com/mbsz.ch/")
    fb_company = fb_company.scrape()
    print(fb_company)
