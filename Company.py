#SOURCES



class CompanyData():
    """
    Class CompanyData():

    """
    def __init__(self, given_name, given_address):
        self.data = {"given_name": given_name,
                    "given_address": given_address,
                    "sources": {}}

    def add_new_source(self, source_name, data={}):
        """NOTE: this will overwrite source if called with same source"""
        self.data["sources"][source_name] = data

    def add_data(self, source_name, key, value):
        self.data["sources"][source_name][key] = value




class Companies():
    """
    analytics()
        returns dict percentage of successfully categorised values
    """
    def __init__(self):
        self.elements = []
    def add(self, company):
        #check type is None or Company
        self.elements.append(company)
        return self.elements
    def len(self):
        return len(self.elements)

    def combine(self, companies2):
        #check same length
        combined = Companies()
        number = 0
        for company in self.elements:
            current_company = Company()
            if company:
                current_company.data = company.data
            else:
                current_company = Company()
            for key, value in current_company.data.items():
                if not value:
                    company2 = companies2.elements[number]
                    if company2:
                        current_company.data[key] = company2.data[key]
            #print(current_company.data)
            combined.add(current_company)
            number += 1
        return combined

    def list_values(self, key):
        list_of_values = []
        for company in self.elements:
            if company:
                try:
                    list_of_values.append(company.data[key])
                except KeyError:
                    print("key not present")
                    return None
            else:
                list_of_values.append(None)
        return list_of_values
    def display(self):
        for company in self.elements:
            if company:
                print()
                for key, value in company.data.items():
                    if value and value != ['']:
                        print(key, ":", value)
    def list_all(self):
        companies_data = []
        for company in self.elements:
            if company:
                companies_data.append(company.data)
            else:
                companies_data.append(Company().data) # i.e. append empty company
        return companies_data


    def analytics(self):
        "calculate percentages found for each element of a company"
        number_fetched = 0
        company_results = Company(all_zero=True)
        for company in self.elements:
            if company:
                number_fetched += 1
                for key, value in company.data.items():
                    if value and value != [""]: #given result is present
                        company_results.data[key] += 1
        for key, value in company_results.data.items():
            company_results.data[key] = 100*company_results.data[key]/self.len()
        total_percent = 100*number_fetched/self.len()
        return self.format_analytics(company_results.data, total_percent)

    def format_analytics(self, results, total_percent):
        string = "RESULTS:"
        string += "{0:.2f}% had result\n".format(total_percent)
        for key, item in results.items():
            string += "{0}:     {1:.2f}%\n".format(key, item)
        print(string)
        return string
