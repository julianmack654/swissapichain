"""DOES A TEXTSEARCH and then a DETAILSSEARCH"""

from Company import CompanyData
from SearchTerm import Search
import requests

G_KEY = "AIzaSyAT8o_RPXNPYW2_905zVN3MlyInjefAgos"
TEXTSEARCH = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
DETAILSSEARCH = "https://maps.googleapis.com/maps/api/place/details/json?"


class GoogleScraper(Search):
    """
    Accepts a search-term. Returns a result
    """
    def __init__(self, search_term):
        self.search_term = self.clean_search_term(search_term)

    def scrape(self):
        """Uses google text search and then Details API search"""
        data = {}
        query_params1 = {"key": G_KEY, "query": self.search_term}
        r = requests.get(url=TEXTSEARCH, params=query_params1)
        json1 = r.json()
        if json1["results"]:
            place_id = json1["results"][0]["place_id"]
            #print(place_id)
            query_params2 = {"key": G_KEY, "placeid": place_id, "region": "CH" }
            r = requests.get(url=DETAILSSEARCH, params=query_params2)
            json2 = r.json()
            if json2["result"]:
                json = json2["result"]
                data["name"] = self.__find_name(json)
                data["address"] = self.__find_address(json)
                data["tel_num"] = self.__find_tel_num(json)
                data["hours"] = self.__find_opening_hrs(json)
                data["website"] = self.__find_website(json)
                data["reviews"] = self.__find_reviews(json)
                data["rating"] = self.__find_ratings(json)
        return data

    def __try_extract(self, json, key):
        try:
            return json[key]
        except KeyError:
            return None

    def __find_name(self, json):
        return self.__try_extract(json, "name")

    def __find_address(self, json):
        return self.__try_extract(json, "formatted_address")

    def __find_tel_num(self, json):
        international = self.__try_extract(json, "international_phone_number")
        formatted  = self.__try_extract(json, "formatted_phone_number")
        if not international and not formatted:
            return None
        else:
            return [international, formatted]

    def __find_opening_hrs(self, json):
        hrs = self.__try_extract(json, "opening_hours")
        return hrs

    def __find_website(self, json):
        return self.__try_extract(json, "website")

    def __find_reviews(self, json):
        reviews = self.__try_extract(json, "reviews")
        return reviews

    def __find_ratings(self, json):
        ratings = self.__try_extract(json, "rating")
        return ratings

if __name__ == "__main__":
    #V_patrice = GoogleScraper("Villet Patrice Chemin du Foron 14")
    #print(V_patrice.extract())
    electroplanet = GoogleScraper("Elektroplanet AG")
    print(electroplanet.scrape())
