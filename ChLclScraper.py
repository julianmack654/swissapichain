import sys
import requests
from bs4 import BeautifulSoup
from Company import CompanyData
from SearchTerm import Search
from fuzzywuzzy import fuzz

CH_LCL = "https://tel.local.ch/en/q?"
CUT_OFF = 40
class ChLclScraper(Search):
    """
    Accepts a company name and location

    """

    def __init__(self, company_name, location):
        #print(company_name)
        self.company_name = self.clean_search_term(company_name)
        self.location = location
        self.params = {"what": company_name, "where": location}
        self.data = {}

    def scrape(self):
        r = requests.get(url=CH_LCL, params=self.params)
        #print("Request made", self.params)
        soup = BeautifulSoup(r.text, 'html.parser')
        #3 options:
        if "No results" in soup.find("title").text: #1) No results
            return self.data
        elif len(soup.find_all("div", class_="detail-entry")) > 0: #2) check result page reached
            #print("page reached")
            self.scrape_chlcl_pg(soup)
        elif len(soup.find_all("div", class_="listing-container")) > 0: #3) search results given - fuzzy match to choose best fit
            #print("search_results?")
            results = soup.find_all("div", class_="listing-address")
            num_results = len(results)
            if num_results > 5:
                num_results = 5
                results = results[:6]
            scores = []
            for result in results:
                address = result.text.strip()
                score = fuzz.token_sort_ratio(address, self.location)
                scores.append(score)
            max_value = max(scores)
            max_index = scores.index(max_value)
            if max_value > CUT_OFF:
                name = soup.find_all("h2", class_="listing-title-container")[max_index].text.strip()
                if fuzz.partial_ratio(name, self.company_name) > CUT_OFF:
                    url = soup.find_all("a", class_="listing-link")[max_index]["href"]
                    r = requests.get(url=url)
                    soup = BeautifulSoup(r.text, 'html.parser')
                    self.scrape_chlcl_pg(soup)
        return self.data

    def scrape_chlcl_pg(self, soup):
        data = self.data
        data["name"] = soup.find("h2", class_="detail-entry-title-container").text.strip()
        sectors = soup.find("div", class_="detail-header-details").text.strip()
        data["sectors"] = sectors.split("•")
        vcard = soup.find_all("div", class_= "vcard")[0].text.strip()
        data["website"] = self.get_value_from_vcard(vcard, "Website")
        data["tel_num"] = self.get_value_from_vcard(vcard, "Phone")
        data["email"] = self.get_value_from_vcard(vcard, "Email")
        data["address"] = self.get_value_from_vcard(vcard, "Address")
        opening_hours = soup.find_all("div", class_= "opening-hours")
        try:
            open_hrs = opening_hours[0].text
            data["open_hours"] = open_hrs
        except KeyError:
            pass
        self.data = data
        return data

    def get_value_from_vcard(self, vcard, value):
        if value in vcard:
            _, tl = vcard.split(value)
            tl = tl.strip()
            try:
                ans, _ = tl.split("\n\n", 1)
            except ValueError:
                ans = tl
            return ans

if __name__ == "__main__":
    #single_search_result = ChLclScraper("BLANCO Profess  N iederlass", "Industriestr. 13")
    #single = single_search_result.extract()
    #print(single.data)
    #no_results = ChLclScraper("Gonnet-Consulting GmbH c/o Ignacio Gast", "Eugen-Huber-Strasse 50")
    #print(no_results.extract())
    #page_extract.extract()
    two_search_results = ChLclScraper("Aatest", "Niederlenzer Kirchweg")
    two = two_search_results.scrape()
    print(two)
