import sys
import requests
from bs4 import BeautifulSoup
from Company import CompanyData
from SearchTerm import Search
from fuzzywuzzy import fuzz
CUT_OFF = 40

EASYM_CH = "https://www.easymonitoring.ch/suche.aspx?"

class EasyMonitoring(Search):
    """
    Accepts a company name and location
    """

    def __init__(self, company_name, location):
        company_name = self.clean_search_term(company_name)
        self.location = location
        self.params = {"type": "firma", "keyword": company_name}
        self.data = {}

    def scrape(self):
        r = requests.get(url=EASYM_CH, params=self.params)
        soup = BeautifulSoup(r.text, 'html.parser')
        #3 options:
        if len(soup.find_all("div", class_="company-page-title")) > 0: #1) result page reached
            self.__scrape_EM_pg(soup)
        elif len(soup.find_all("tr")) > 0: #2) search results given - use fuzzy matching to find best
            search_links = soup.find("table").find_all("a")
            num_results = len(search_links)
            if num_results > 5:
                num_results = 5
                search_links = search_links[:num_results] #take first 5 terms
            link_results = []
            #print(search_links)
            for link in search_links:
                r = requests.get(url=link["href"])
                soup = BeautifulSoup(r.text, 'html.parser')
                address = self.__get_addr_from_page(soup)
                if address:
                    score = fuzz.token_sort_ratio(address, self.location)
                else:
                    score = 0
                result = {"score": score, "soup": soup}
                link_results.append(result)
            sorted_link_results = sorted(link_results, key=lambda k: k['score'])
            result = sorted_link_results[-1]
            if result["score"] > CUT_OFF:
                self.__scrape_EM_pg(soup)
        else: #3) No results
            pass
        return self.data

    def __get_addr_from_page(self, soup):
        def get_address(text):
            if "Domizil (Karte)" in text and "Sitz" in text:
                _, tl = text.split("Domizil (Karte)")
                address, _ = tl.split("Sitz", 1)
                return address
            else:
                return None
        return self.__get_info_from_table(soup, get_address, 0)

    def __get_capital_from_page(self, soup):
        def get_Kapital(text):
            if "Kapital" in text:
                try:
                    _, capital = text.split("Kapital")
                    capital, _ = capital.split(".--", 1)
                    capital = int(capital.replace("\'", "").strip())
                    return capital
                except:
                    return None
            else:
                return None

        return self.__get_info_from_table(soup, get_Kapital, 0)

    def __get_company_number_from_page(self, soup):
        def get_co_num(text):
            if "UID" in text:
                _, tl = text.split("UID")
                address, _ = tl.split("CH-Nummer", 1)
                return address.strip()
            else:
                return None
        return self.__get_info_from_table(soup, get_co_num, 1)

    def __get_company_description(self, soup):
        def get_co_descr(text):
            if "E-Mail Zweck" in text:
                _, tl = text.split("E-Mail Zweck")
                address, _ = tl.split("UID", 1)
                return address.strip()
            else:
                return None
        return self.__get_info_from_table(soup, get_co_descr, 1)

    def __get_info_from_table(self, soup, fn, table_number):
        tables = soup.find_all("table", class_="table-account")
        if tables:
            table0 = tables[0]
            table1 = tables[1] #second table
            if table_number == 0:
                table = table0
            elif table_number == 1:
                table = table1
            else:
                print("Invalid table number provided")
                sys.exit(1)
            words = []
            for word in table.text.split(" "):
                if word != "":
                    words.append(word.replace("\n", " ").strip())
            text = " ".join(words)
            out = []
            for word in text.split():
                out.append(word.strip())
            text = " ".join(out)
            return fn(text)
        return None

    def __scrape_EM_pg(self, soup):
        """Scrapes an EM page"""
        self.data["address"] = self.__get_addr_from_page(soup)
        self.data["Capital"] = self.__get_capital_from_page(soup)
        self.data["Company Number"] = self.__get_company_number_from_page(soup)
        self.data["Description"] = self.__get_company_description(soup)
        return self.data

    def __set_full_company_info(self):
        name = address = seat = canton = funds = description = CH_number = incorporation_date = True
        self.company = Company(name=name, address=address, seat=seat,
                            canton=canton, funds = funds, description = description,
                            CH_number = CH_number, incorporation_date = incorporation_date)
        return self.company

if __name__ == "__main__":

    single_search_result = EasyMonitoring("Elektroplanet AG", "Staffelbacherstrasse 34")
    single = single_search_result.scrape()
    print(single)
    #page_extract = ChLclScraper("BLANCO", "Industriestr. 13")
    #page_extract.extract()
