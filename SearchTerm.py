import string

class Search():
    def clean_search_term(self, search_term):
        search_term = search_term.lower()
        search_term.replace("gmbh", "")
        search_term.replace("ag", "")
        return search_term

def remove_punct(text):
    """Removes punctuation"""
    return text.translate(text.maketrans("", "", string.punctuation))
