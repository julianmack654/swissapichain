# Swiss Data Automated Collection


##Batch Scraper tool
This will form a CompanyData object with data from three sources:
Google
LocalCh (yellow pages eq.)
EasyMonitoring (companies Data aggregation site).

Results with be saved in excel sheet.
It can be called with the following:

```
python3 chainAPICalls.py
```

Note: this will run a test xls. Update xls input path in header of "chainAPICalls" to make specific. You will have to modify the script if the headings are different to 'Firm' and 'Street Address'.


###Json/Dictionary output:
If prefered - the output is given as a nested dictionary from function ```chain_swiss_API_calls(company_name, company_address)``` in chainAPICalls.py and this can be printed clearly by converting to a json:

```
import json
company_data = chain_swiss_API_calls(company_name, company_address)
print(json.dumps(company_data, indent=3))
```

##Individual scrapers
Each scraper can also be used independently with nested dictionary output.
###Google/ChLcl/EasyMonitoring:
```
from GoogleScraper import GoogleScraper
data = GoogleScraper(search_term).scrape() #nested dictionary

from ChLclScraper import ChLclScraper
data = ChLclScraper(company_name, location).scrape() #nested dictionary

from EasyMonitoring import EasyMonitoring
data = EasyMonitoring(company_name, location).scrape() #nested dictionary
```
###Facebook Scraper
Facebook scraper requires fb url from phantombuster (or alternative) so is not in the batch scrape tool.
```
from Facebook Scraper import FacebookScraperWrapper
data = FacebookScraperWrapper(url, full_address=None, street_address=None).scrape()
#full_address and street_address are both optional but one must be passed to get results.
#(they are used for validation of the phantombuster link)
```
