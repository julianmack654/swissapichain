from Company import CompanyData
from GoogleScraper import GoogleScraper
from ChLclScraper import ChLclScraper
from EasyMonitoring import EasyMonitoring
import pandas as pd
import json
import sys

#XLS workbooks
SWISS_COMPANIES_XLS = 'data/swisscompaniesbools.xlsx'

#XLS configuration - CHANGE THESE
INPUT_XLSX = SWISS_COMPANIES_XLS
INPUT_SHEET = "Names_only"
MAX_NUMBER_RESULTS_TO_PARSE = 5 #CHANGE THIS
OUTPUT_XLSX = "data/results.xlsx"
OUTPUT_SHEET = "Sheet 1"

GOOGLE = "google"
LCL_CH = "lcl_CH"
EASY_MONITOR = "EasyMonitor"

def main():
    """Read info into dataframe. Make sure you have correct columns"""
    df_start = pd.read_excel(SWISS_COMPANIES_XLS, sheet_name=INPUT_SHEET)
    df_final = pd.DataFrame()
    for index, row in df_start.iterrows():
        name = row["Firm"]
        address = row["Street Address"]
        company_data = chain_swiss_API_calls(name, address) #this is a nested dict
        company_series = convert_to_series(company_data)    #this is a series
        df_final = pd.concat([df_final, company_series], axis=1, sort=False)
        if index == MAX_NUMBER_RESULTS_TO_PARSE:
            break
    df = df_final.transpose()
    xls_writer = pd.ExcelWriter(OUTPUT_XLSX)
    df.to_excel(xls_writer, OUTPUT_SHEET)
    xls_writer.save()


def chain_swiss_API_calls(name, address):
    company = CompanyData(name, address)
    sources = {
        GOOGLE: GoogleScraper(name + " " + address),
        EASY_MONITOR: EasyMonitoring(name, address),
        LCL_CH: ChLclScraper(name, address)}
    for key, source in sources.items():
        company.add_new_source(key, source.scrape())
    #print(json.dumps(company.data, indent=3))
    return company.data


def convert_to_series(company_data):
    row = {}
    for key, value in company_data.items():
        if type(value) == str:
            row[key] = value
        elif key == "sources":
            for source_name, results in value.items():
                if results:
                    for data_point_name, data_point in results.items():
                        if data_point:
                            new_key = "{}.{}".format(data_point_name, source_name)
                            row[new_key] = data_point
    ds = pd.Series(row)
    return ds


if __name__ == "__main__":
    main()
    #chain_swiss_API_calls("Elektroplanet GmbH", "Industriestrasse 2")
